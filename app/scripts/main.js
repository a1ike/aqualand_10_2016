$(document).ready(function() {

  $('html, body').animate({
    scrollTop: 0
  }, 'slow');

  $('.carousel').carousel({
    pause: true,
    interval: false
  });

  $('.portfolio-card').slice(0, 6).show();
  $('#loadMore').on('click', function(e) {
    e.preventDefault();
    $('.portfolio-card:hidden').slice(0, 3).slideDown();
    if ($('.portfolio-card:hidden').length == 0) {
      $('#load').fadeOut('slow');
    }
  });

  $('#fullpage').fullpage({
    //Navigation
    menu: '#menu',
    lockAnchors: false,
    anchors: ['firstPage', 'secondPage', 'thirdPage', 'fourthPage'],
    navigation: true,
    navigationPosition: 'right',
    navigationTooltips: ['Превью', 'Услуги', 'Подробнее', 'Контакты'],
    showActiveTooltip: false,
    slidesNavigation: true,
    slidesNavPosition: 'bottom',

    //Scrolling
    css3: true,
    scrollingSpeed: 1000,
    autoScrolling: true,
    fitToSection: true,
    fitToSectionDelay: 1000,
    scrollBar: false,
    easing: 'easeInOutCubic',
    easingcss3: 'ease',
    loopBottom: false,
    loopTop: false,
    loopHorizontal: true,
    continuousVertical: false,
    continuousHorizontal: false,
    scrollHorizontally: false,
    interlockedSlides: false,
    resetSliders: false,
    fadingEffect: false,
    normalScrollElements: '#element1, .element2',
    scrollOverflow: true,
    scrollOverflowOptions: null,
    touchSensitivity: 15,
    normalScrollElementTouchThreshold: 5,
    bigSectionsDestination: null,
    responsiveWidth: 768,

    //Accessibility
    keyboardScrolling: true,
    animateAnchor: true,
    recordHistory: true,

    //Design
    controlArrows: true,
    verticalCentered: false,
    sectionsColor: ['#fff'],
    paddingTop: '0',
    paddingBottom: '0',
    fixedElements: '#header, .footer',

    //Custom selectors
    sectionSelector: '.section',
    slideSelector: '.slide',

    //events
    onLeave: function(index, nextIndex, direction) {},
    afterLoad: function(anchorLink, index) {},
    afterRender: function() {},
    afterResize: function() {},
    afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex) {},
    onSlideLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex) {}
  });

  //owl
  $('#owl-seo').owlCarousel({

    navigation: true, // Show next and prev buttons
    slideSpeed: 300,
    pagination: false,
    paginationSpeed: 400,
    singleItem: true,

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false

    navigationText: ['<img src=\'images/arrow_left.png\'>', '<img src=\'images/arrow_right.png\'>']

  });

});